
# Text-To-Sing Lyric Splitter

This project shares the code required to split the words of a string into syllables in the exact way the Text-To-Sing API needs it.


## Using this code

To make use of this library, you'll have to import the `hyphenator.js` file first, and then the `ttsing-lyrics.js` library.

Sample code using this code:

```javascript
$("#lyrics").change( (evnt) => {
    $("#split-lyrics").text(splitLineIntoSyllables('en', evnt.target.value))
})
```

For an input such as **"Deck the halls with bough of holly"**, you'll get an output like: `deck,the,halls,with,bough,of,hol,ly`


