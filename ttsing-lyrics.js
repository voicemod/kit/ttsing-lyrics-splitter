
function splitLineIntoSyllables(language, inputText) {
    if (language == 'en' || language == 'es') {

      var syllableGroups = hyphenatePhrase(inputText);
      var arraySyllables = [];

      for(var i=0; i < syllableGroups.length; i++) {

        if (typeof(syllableGroups[i]) === "string") {
          arraySyllables.push(syllableGroups[i]);
        }

        if (typeof(syllableGroups[i]) === "object") {
          var aWord = syllableGroups[i];
          for(var j=0; j < aWord.length; j++) {
            arraySyllables.push(aWord[j]);
          }

        }
      }

      return arraySyllables;
    }
}